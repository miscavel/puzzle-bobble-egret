export function getAsset(url: string) {
  if (!url) return '';

  // TO-DO: use actual tenant
  return `https://cf.shopee.co.id/file/${url}`;
}