import { ASSET_KEY } from '../Enum/Asset';

const atlasCollection = {
  [ASSET_KEY.ATLAS_SHEET_1]: {
    frames: {
      gameplay_booster_area_bomb_explosion000: {
        frame: {
          x: 1005,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion001: {
        frame: {
          x: 501,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion002: {
        frame: {
          x: 501,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion003: {
        frame: {
          x: 504,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion004: {
        frame: {
          x: 0,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion005: {
        frame: {
          x: 504,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion006: {
        frame: {
          x: 1005,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_booster_area_bomb_explosion007: {
        frame: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_warning_edge_effect000: {
        frame: {
          x: 1506,
          y: 0,
          w: 503,
          h: 1001
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 503,
          h: 1001
        },
        sourceSize: {
          w: 503,
          h: 1001
        }
      },
      gameplay_warning_edge_effect001: {
        frame: {
          x: 1506,
          y: 1002,
          w: 503,
          h: 1001
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 503,
          h: 1001
        },
        sourceSize: {
          w: 503,
          h: 1001
        }
      },
      gameplay_warning_edge_effect002: {
        frame: {
          x: 1002,
          y: 0,
          w: 503,
          h: 1001
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 503,
          h: 1001
        },
        sourceSize: {
          w: 503,
          h: 1001
        }
      },
      gameplay_warning_edge_effect003: {
        frame: {
          x: 0,
          y: 1002,
          w: 503,
          h: 1001
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 503,
          h: 1001
        },
        sourceSize: {
          w: 503,
          h: 1001
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_1.png',
      size: {
        w: 2009,
        h: 2003
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_2]: {
    frames: {
      gameplay_booster_rainbow_ball_lightning000: {
        frame: {
          x: 438,
          y: 277,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning001: {
        frame: {
          x: 438,
          y: 777,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning002: {
        frame: {
          x: 1692,
          y: 500,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning003: {
        frame: {
          x: 715,
          y: 1000,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning004: {
        frame: {
          x: 539,
          y: 777,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning005: {
        frame: {
          x: 715,
          y: 500,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning006: {
        frame: {
          x: 539,
          y: 277,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning007: {
        frame: {
          x: 1692,
          y: 0,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_ball_lightning008: {
        frame: {
          x: 715,
          y: 0,
          w: 100,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 100,
          h: 499
        },
        sourceSize: {
          w: 100,
          h: 499
        }
      },
      gameplay_booster_rainbow_screen_edge_fx000: {
        frame: {
          x: 1254,
          y: 0,
          w: 160,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 1579
        },
        sourceSize: {
          w: 160,
          h: 1579
        }
      },
      gameplay_booster_rainbow_screen_edge_fx001: {
        frame: {
          x: 1093,
          y: 0,
          w: 160,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 1579
        },
        sourceSize: {
          w: 160,
          h: 1579
        }
      },
      gameplay_booster_rainbow_screen_edge_fx002: {
        frame: {
          x: 1793,
          y: 0,
          w: 160,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 1579
        },
        sourceSize: {
          w: 160,
          h: 1579
        }
      },
      gameplay_booster_rainbow_screen_edge_fx003: {
        frame: {
          x: 0,
          y: 0,
          w: 160,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 1579
        },
        sourceSize: {
          w: 160,
          h: 1579
        }
      },
      gameplay_fireworks_cool000: {
        frame: {
          x: 1385,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool001: {
        frame: {
          x: 816,
          y: 277,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool002: {
        frame: {
          x: 277,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool003: {
        frame: {
          x: 161,
          y: 554,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool004: {
        frame: {
          x: 1415,
          y: 831,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool005: {
        frame: {
          x: 816,
          y: 831,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool006: {
        frame: {
          x: 1108,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool007: {
        frame: {
          x: 1415,
          y: 0,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool008: {
        frame: {
          x: 161,
          y: 1108,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool009: {
        frame: {
          x: 816,
          y: 554,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool010: {
        frame: {
          x: 1415,
          y: 554,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_cool011: {
        frame: {
          x: 438,
          y: 1277,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm000: {
        frame: {
          x: 161,
          y: 277,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm001: {
        frame: {
          x: 831,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm002: {
        frame: {
          x: 438,
          y: 0,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm003: {
        frame: {
          x: 1662,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm004: {
        frame: {
          x: 1415,
          y: 277,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm005: {
        frame: {
          x: 161,
          y: 0,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm006: {
        frame: {
          x: 816,
          y: 0,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm007: {
        frame: {
          x: 0,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm008: {
        frame: {
          x: 161,
          y: 831,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm009: {
        frame: {
          x: 816,
          y: 1108,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm010: {
        frame: {
          x: 1415,
          y: 1108,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      },
      gameplay_fireworks_warm011: {
        frame: {
          x: 554,
          y: 1580,
          w: 276,
          h: 276
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 276,
          h: 276
        },
        sourceSize: {
          w: 276,
          h: 276
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_2.png',
      size: {
        w: 1953,
        h: 1856
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_3]: {
    frames: {
      pop_Orange000: {
        frame: {
          x: 790,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Orange001: {
        frame: {
          x: 0,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Orange002: {
        frame: {
          x: 790,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Orange003: {
        frame: {
          x: 1586,
          y: 368,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Orange004: {
        frame: {
          x: 790,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Red000: {
        frame: {
          x: 395,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Red001: {
        frame: {
          x: 395,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Red002: {
        frame: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Red003: {
        frame: {
          x: 0,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Red004: {
        frame: {
          x: 395,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Stone000: {
        frame: {
          x: 1586,
          y: 1498,
          w: 400,
          h: 400
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 400,
          h: 400
        },
        sourceSize: {
          w: 400,
          h: 400
        }
      },
      pop_Stone001: {
        frame: {
          x: 1185,
          y: 802,
          w: 400,
          h: 400
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 400,
          h: 400
        },
        sourceSize: {
          w: 400,
          h: 400
        }
      },
      pop_Stone002: {
        frame: {
          x: 1185,
          y: 401,
          w: 400,
          h: 400
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 400,
          h: 400
        },
        sourceSize: {
          w: 400,
          h: 400
        }
      },
      pop_Stone003: {
        frame: {
          x: 1185,
          y: 0,
          w: 400,
          h: 400
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 400,
          h: 400
        },
        sourceSize: {
          w: 400,
          h: 400
        }
      },
      pop_Vine000: {
        frame: {
          x: 412,
          y: 1182,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine001: {
        frame: {
          x: 0,
          y: 1182,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine002: {
        frame: {
          x: 0,
          y: 1550,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine003: {
        frame: {
          x: 1586,
          y: 762,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine004: {
        frame: {
          x: 412,
          y: 1550,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine005: {
        frame: {
          x: 824,
          y: 1550,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine006: {
        frame: {
          x: 1586,
          y: 1130,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      },
      pop_Vine007: {
        frame: {
          x: 1586,
          y: 0,
          w: 411,
          h: 367
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 411,
          h: 367
        },
        sourceSize: {
          w: 411,
          h: 367
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_3.png',
      size: {
        w: 1997,
        h: 1917
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_4]: {
    frames: {
      gameplay_booster_fire_ball_blast002: {
        frame: {
          x: 877,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast003: {
        frame: {
          x: 1359,
          y: 841,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast004: {
        frame: {
          x: 1761,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast005: {
        frame: {
          x: 636,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast006: {
        frame: {
          x: 1359,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast007: {
        frame: {
          x: 1761,
          y: 841,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_shoot000: {
        frame: {
          x: 1600,
          y: 0,
          w: 160,
          h: 539
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 539
        },
        sourceSize: {
          w: 160,
          h: 539
        }
      },
      gameplay_booster_fire_ball_shoot001: {
        frame: {
          x: 1600,
          y: 1080,
          w: 160,
          h: 539
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 539
        },
        sourceSize: {
          w: 160,
          h: 539
        }
      },
      gameplay_booster_fire_ball_shoot002: {
        frame: {
          x: 1600,
          y: 540,
          w: 160,
          h: 539
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 539
        },
        sourceSize: {
          w: 160,
          h: 539
        }
      },
      gameplay_bubble_highlight: {
        frame: {
          x: 1229,
          y: 1629,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_burnt_ball000: {
        frame: {
          x: 907,
          y: 1629,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball001: {
        frame: {
          x: 241,
          y: 394,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball002: {
        frame: {
          x: 1600,
          y: 1620,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball003: {
        frame: {
          x: 1185,
          y: 841,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball004: {
        frame: {
          x: 746,
          y: 1629,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball005: {
        frame: {
          x: 1141,
          y: 1235,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball006: {
        frame: {
          x: 1068,
          y: 1629,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      gameplay_burnt_ball007: {
        frame: {
          x: 402,
          y: 394,
          w: 160,
          h: 319
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 319
        },
        sourceSize: {
          w: 160,
          h: 319
        }
      },
      pop_Pink000: {
        frame: {
          x: 746,
          y: 1235,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Pink001: {
        frame: {
          x: 395,
          y: 841,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Pink002: {
        frame: {
          x: 790,
          y: 841,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Pink003: {
        frame: {
          x: 0,
          y: 841,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Pink004: {
        frame: {
          x: 241,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      general_image_glow_no_particle: {
        frame: {
          x: 0,
          y: 1235,
          w: 745,
          h: 745
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 745,
          h: 745
        },
        sourceSize: {
          w: 745,
          h: 745
        }
      },
      gameplay_booster_fire_ball_blast000: {
        frame: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      },
      gameplay_booster_fire_ball_blast001: {
        frame: {
          x: 1118,
          y: 0,
          w: 240,
          h: 840
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 240,
          h: 840
        },
        sourceSize: {
          w: 240,
          h: 840
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_4.png',
      size: {
        w: 2001,
        h: 1980
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_5]: {
    frames: {
      gameplay_booster_time_freeze_overlay: {
        frame: {
          x: 0,
          y: 233,
          w: 750,
          h: 1600
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 750,
          h: 1600
        },
        sourceSize: {
          w: 750,
          h: 1600
        }
      },
      gameplay_booster_time_freeze_snowflake: {
        frame: {
          x: 0,
          y: 0,
          w: 80,
          h: 80
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 80,
          h: 80
        },
        sourceSize: {
          w: 80,
          h: 80
        }
      },
      gameplay_booster_time_freeze_sparkle: {
        frame: {
          x: 0,
          y: 81,
          w: 80,
          h: 80
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 80,
          h: 80
        },
        sourceSize: {
          w: 80,
          h: 80
        }
      },
      gameplay_glow_effect: {
        frame: {
          x: 751,
          y: 1031,
          w: 745,
          h: 745
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 745,
          h: 745
        },
        sourceSize: {
          w: 745,
          h: 745
        }
      },
      gameplay_safety_line: {
        frame: {
          x: 0,
          y: 162,
          w: 750,
          h: 70
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 750,
          h: 70
        },
        sourceSize: {
          w: 750,
          h: 70
        }
      },
      pop_Purple000: {
        frame: {
          x: 1502,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Purple001: {
        frame: {
          x: 1502,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Purple002: {
        frame: {
          x: 751,
          y: 637,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Purple003: {
        frame: {
          x: 1502,
          y: 1182,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Purple004: {
        frame: {
          x: 1502,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      gameplay_booster_line_bomb_overlay: {
        frame: {
          x: 751,
          y: 0,
          w: 750,
          h: 636
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 750,
          h: 636
        },
        sourceSize: {
          w: 750,
          h: 636
        }
      },
      gameplay_booster_time_freeze_icon_big: {
        frame: {
          x: 1146,
          y: 637,
          w: 310,
          h: 307
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 310,
          h: 307
        },
        sourceSize: {
          w: 310,
          h: 307
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_5.png',
      size: {
        w: 1896,
        h: 1833
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_6]: {
    frames: {
      gameplay_booster_fire_ball_sprite002: {
        frame: {
          x: 123,
          y: 1596,
          w: 110,
          h: 109
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 109
        },
        sourceSize: {
          w: 110,
          h: 109
        }
      },
      gameplay_booster_line_bomb_explosion000: {
        frame: {
          x: 0,
          y: 1723,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion001: {
        frame: {
          x: 1001,
          y: 554,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion002: {
        frame: {
          x: 1001,
          y: 1520,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion003: {
        frame: {
          x: 1001,
          y: 1359,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion004: {
        frame: {
          x: 1001,
          y: 0,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion005: {
        frame: {
          x: 1001,
          y: 1037,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion006: {
        frame: {
          x: 1001,
          y: 715,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion007: {
        frame: {
          x: 1001,
          y: 876,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion008: {
        frame: {
          x: 1001,
          y: 393,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion009: {
        frame: {
          x: 1001,
          y: 1681,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion010: {
        frame: {
          x: 1001,
          y: 1198,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_booster_line_bomb_explosion011: {
        frame: {
          x: 1001,
          y: 161,
          w: 1000,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 1000,
          h: 160
        },
        sourceSize: {
          w: 1000,
          h: 160
        }
      },
      gameplay_combo_edge_effect000: {
        frame: {
          x: 873,
          y: 0,
          w: 122,
          h: 1580
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 122,
          h: 1580
        },
        sourceSize: {
          w: 122,
          h: 1580
        }
      },
      gameplay_combo_edge_effect001: {
        frame: {
          x: 750,
          y: 0,
          w: 122,
          h: 1580
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 122,
          h: 1580
        },
        sourceSize: {
          w: 122,
          h: 1580
        }
      },
      gameplay_combo_edge_effect002: {
        frame: {
          x: 0,
          y: 142,
          w: 122,
          h: 1580
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 122,
          h: 1580
        },
        sourceSize: {
          w: 122,
          h: 1580
        }
      },
      gameplay_safety_line_danger000: {
        frame: {
          x: 1001,
          y: 322,
          w: 749,
          h: 70
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 749,
          h: 70
        },
        sourceSize: {
          w: 749,
          h: 70
        }
      },
      gameplay_safety_line_danger001: {
        frame: {
          x: 0,
          y: 0,
          w: 749,
          h: 70
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 749,
          h: 70
        },
        sourceSize: {
          w: 749,
          h: 70
        }
      },
      gameplay_safety_line_danger002: {
        frame: {
          x: 0,
          y: 71,
          w: 749,
          h: 70
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 749,
          h: 70
        },
        sourceSize: {
          w: 749,
          h: 70
        }
      },
      gameplay_vine_show000: {
        frame: {
          x: 403,
          y: 614,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show001: {
        frame: {
          x: 403,
          y: 378,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show002: {
        frame: {
          x: 123,
          y: 1360,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show003: {
        frame: {
          x: 123,
          y: 142,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show004: {
        frame: {
          x: 123,
          y: 378,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show005: {
        frame: {
          x: 123,
          y: 614,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show006: {
        frame: {
          x: 403,
          y: 142,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_vine_show007: {
        frame: {
          x: 123,
          y: 1124,
          w: 279,
          h: 235
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 279,
          h: 235
        },
        sourceSize: {
          w: 279,
          h: 235
        }
      },
      gameplay_booster_fire_ball_sprite000: {
        frame: {
          x: 234,
          y: 1596,
          w: 110,
          h: 109
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 109
        },
        sourceSize: {
          w: 110,
          h: 109
        }
      },
      gameplay_booster_fire_ball_sprite001: {
        frame: {
          x: 123,
          y: 1014,
          w: 110,
          h: 109
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 109
        },
        sourceSize: {
          w: 110,
          h: 109
        }
      },
      extra_moveOrange: {
        frame: {
          x: 403,
          y: 1124,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_movePink: {
        frame: {
          x: 123,
          y: 850,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_movePurple: {
        frame: {
          x: 567,
          y: 1124,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_moveRed: {
        frame: {
          x: 287,
          y: 850,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_moveYellow: {
        frame: {
          x: 451,
          y: 850,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_moveBlue: {
        frame: {
          x: 403,
          y: 1360,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_moveGreen: {
        frame: {
          x: 567,
          y: 1360,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_6.png',
      size: {
        w: 2001,
        h: 1883
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_7]: {
    frames: {
      extra_timeOrange: {
        frame: {
          x: 714,
          y: 1288,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timePink: {
        frame: {
          x: 878,
          y: 1288,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timePurple: {
        frame: {
          x: 1042,
          y: 1288,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timeRed: {
        frame: {
          x: 1610,
          y: 0,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timeYellow: {
        frame: {
          x: 1610,
          y: 164,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishBlue: {
        frame: {
          x: 1610,
          y: 328,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishGreen: {
        frame: {
          x: 1610,
          y: 492,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishOrange: {
        frame: {
          x: 1610,
          y: 656,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishPink: {
        frame: {
          x: 1610,
          y: 820,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishPurple: {
        frame: {
          x: 1610,
          y: 984,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishRed: {
        frame: {
          x: 1610,
          y: 1148,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      fishYellow: {
        frame: {
          x: 1610,
          y: 1312,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_booster_area_bomb_icon_disabled: {
        frame: {
          x: 714,
          y: 1452,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_booster_area_bomb_sprite000: {
        frame: {
          x: 1774,
          y: 1226,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_area_bomb_sprite001: {
        frame: {
          x: 1774,
          y: 1337,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_area_bomb_sprite002: {
        frame: {
          x: 1774,
          y: 1448,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_area_bomb_sprite003: {
        frame: {
          x: 1774,
          y: 1559,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_fire_ball_icon_disabled: {
        frame: {
          x: 835,
          y: 1452,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_booster_frame: {
        frame: {
          x: 1109,
          y: 991,
          w: 86,
          h: 86
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 86,
          h: 86
        },
        sourceSize: {
          w: 86,
          h: 86
        }
      },
      gameplay_booster_frame_disabled: {
        frame: {
          x: 1109,
          y: 1078,
          w: 86,
          h: 86
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 86,
          h: 86
        },
        sourceSize: {
          w: 86,
          h: 86
        }
      },
      gameplay_booster_line_bomb_icon_disabled: {
        frame: {
          x: 956,
          y: 1452,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_booster_rainbow_ball_bubble_struck: {
        frame: {
          x: 1077,
          y: 1452,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_booster_rainbow_ball_icon_disabled: {
        frame: {
          x: 1774,
          y: 984,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_booster_time_freeze_icon_disabled: {
        frame: {
          x: 1774,
          y: 1105,
          w: 120,
          h: 120
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 120,
          h: 120
        },
        sourceSize: {
          w: 120,
          h: 120
        }
      },
      gameplay_check_icon: {
        frame: {
          x: 213,
          y: 1500,
          w: 39,
          h: 39
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 39,
          h: 39
        },
        sourceSize: {
          w: 39,
          h: 39
        }
      },
      gameplay_fire_effect000: {
        frame: {
          x: 0,
          y: 0,
          w: 70,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 70,
          h: 1579
        },
        sourceSize: {
          w: 70,
          h: 1579
        }
      },
      gameplay_fire_effect001: {
        frame: {
          x: 71,
          y: 0,
          w: 70,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 70,
          h: 1579
        },
        sourceSize: {
          w: 70,
          h: 1579
        }
      },
      gameplay_fire_effect002: {
        frame: {
          x: 142,
          y: 0,
          w: 70,
          h: 1579
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 70,
          h: 1579
        },
        sourceSize: {
          w: 70,
          h: 1579
        }
      },
      gameplay_tutorial_hand: {
        frame: {
          x: 1109,
          y: 894,
          w: 76,
          h: 96
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 76,
          h: 96
        },
        sourceSize: {
          w: 76,
          h: 96
        }
      },
      pop_Blue000: {
        frame: {
          x: 714,
          y: 500,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Blue001: {
        frame: {
          x: 714,
          y: 894,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Blue002: {
        frame: {
          x: 1215,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Blue003: {
        frame: {
          x: 1215,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Blue004: {
        frame: {
          x: 1215,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Chain000: {
        frame: {
          x: 213,
          y: 0,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      pop_Chain001: {
        frame: {
          x: 213,
          y: 500,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      pop_Chain002: {
        frame: {
          x: 213,
          y: 1000,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      pop_Chain003: {
        frame: {
          x: 714,
          y: 0,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      pop_Green000: {
        frame: {
          x: 1215,
          y: 1182,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Green001: {
        frame: {
          x: 0,
          y: 1580,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Green002: {
        frame: {
          x: 395,
          y: 1580,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Green003: {
        frame: {
          x: 790,
          y: 1580,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Green004: {
        frame: {
          x: 1185,
          y: 1580,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      score_multiplierBlue: {
        frame: {
          x: 1610,
          y: 1476,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierGreen: {
        frame: {
          x: 1610,
          y: 1640,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierOrange: {
        frame: {
          x: 1610,
          y: 1804,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierPink: {
        frame: {
          x: 1774,
          y: 0,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierPurple: {
        frame: {
          x: 1774,
          y: 164,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierRed: {
        frame: {
          x: 1774,
          y: 328,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      score_multiplierYellow: {
        frame: {
          x: 1774,
          y: 492,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timeBlue: {
        frame: {
          x: 1774,
          y: 656,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      extra_timeGreen: {
        frame: {
          x: 1774,
          y: 820,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_7.png',
      size: {
        w: 1937,
        h: 1973
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_8]: {
    frames: {
      gameplay_booster_line_bomb_rocket002: {
        frame: {
          x: 0,
          y: 1637,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket003: {
        frame: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket004: {
        frame: {
          x: 0,
          y: 493,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket005: {
        frame: {
          x: 963,
          y: 1798,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket006: {
        frame: {
          x: 0,
          y: 1476,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket007: {
        frame: {
          x: 0,
          y: 654,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket008: {
        frame: {
          x: 0,
          y: 1798,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket009: {
        frame: {
          x: 0,
          y: 815,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_sprite000: {
        frame: {
          x: 641,
          y: 815,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_line_bomb_sprite001: {
        frame: {
          x: 1282,
          y: 1637,
          w: 110,
          h: 110
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 110,
          h: 110
        },
        sourceSize: {
          w: 110,
          h: 110
        }
      },
      gameplay_booster_rainbow_ball_activated000: {
        frame: {
          x: 802,
          y: 1798,
          w: 160,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 160
        },
        sourceSize: {
          w: 160,
          h: 160
        }
      },
      gameplay_booster_rainbow_ball_activated001: {
        frame: {
          x: 1604,
          y: 1798,
          w: 160,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 160
        },
        sourceSize: {
          w: 160,
          h: 160
        }
      },
      gameplay_booster_rainbow_ball_activated002: {
        frame: {
          x: 641,
          y: 1798,
          w: 160,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 160,
          h: 160
        },
        sourceSize: {
          w: 160,
          h: 160
        }
      },
      gameplay_booster_reminder_glow: {
        frame: {
          x: 641,
          y: 394,
          w: 220,
          h: 220
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 220,
          h: 220
        },
        sourceSize: {
          w: 220,
          h: 220
        }
      },
      gameplay_bubble_landing_spot: {
        frame: {
          x: 0,
          y: 322,
          w: 170,
          h: 170
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 170,
          h: 170
        },
        sourceSize: {
          w: 170,
          h: 170
        }
      },
      gameplay_collected_ball_effect: {
        frame: {
          x: 1431,
          y: 1500,
          w: 274,
          h: 274
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 274,
          h: 274
        },
        sourceSize: {
          w: 274,
          h: 274
        }
      },
      gameplay_fish_destroy000: {
        frame: {
          x: 1431,
          y: 500,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      gameplay_fish_destroy001: {
        frame: {
          x: 1431,
          y: 0,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      gameplay_fish_destroy002: {
        frame: {
          x: 0,
          y: 976,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      gameplay_fish_destroy003: {
        frame: {
          x: 1431,
          y: 1000,
          w: 500,
          h: 499
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 499
        },
        sourceSize: {
          w: 500,
          h: 499
        }
      },
      gameplay_fish_glowBlue: {
        frame: {
          x: 335,
          y: 322,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowGreen: {
        frame: {
          x: 665,
          y: 976,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowOrange: {
        frame: {
          x: 862,
          y: 394,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowPink: {
        frame: {
          x: 501,
          y: 976,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowPurple: {
        frame: {
          x: 829,
          y: 976,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowRed: {
        frame: {
          x: 805,
          y: 615,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_fish_glowYellow: {
        frame: {
          x: 665,
          y: 1140,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_mascot_thought_bubble: {
        frame: {
          x: 641,
          y: 1476,
          w: 212,
          h: 136
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 212,
          h: 136
        },
        sourceSize: {
          w: 212,
          h: 136
        }
      },
      pop_Yellow000: {
        frame: {
          x: 1036,
          y: 788,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Yellow001: {
        frame: {
          x: 1036,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Yellow002: {
        frame: {
          x: 1036,
          y: 394,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Yellow003: {
        frame: {
          x: 1036,
          y: 1182,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      pop_Yellow004: {
        frame: {
          x: 641,
          y: 0,
          w: 394,
          h: 393
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 394,
          h: 393
        },
        sourceSize: {
          w: 394,
          h: 393
        }
      },
      trajectory_guideBlue: {
        frame: {
          x: 829,
          y: 1140,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guideGreen: {
        frame: {
          x: 171,
          y: 322,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guideOrange: {
        frame: {
          x: 501,
          y: 1140,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guidePink: {
        frame: {
          x: 829,
          y: 1304,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guidePurple: {
        frame: {
          x: 501,
          y: 1304,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guideRed: {
        frame: {
          x: 665,
          y: 1304,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      trajectory_guideYellow: {
        frame: {
          x: 641,
          y: 615,
          w: 163,
          h: 163
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 163,
          h: 163
        },
        sourceSize: {
          w: 163,
          h: 163
        }
      },
      gameplay_booster_line_bomb_rocket000: {
        frame: {
          x: 0,
          y: 161,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      },
      gameplay_booster_line_bomb_rocket001: {
        frame: {
          x: 641,
          y: 1637,
          w: 640,
          h: 160
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 640,
          h: 160
        },
        sourceSize: {
          w: 640,
          h: 160
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_8.png',
      size: {
        w: 1931,
        h: 1958
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_9]: {
    frames: {
      gameplay_chameleon_transform_color_part002: {
        frame: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part003: {
        frame: {
          x: 0,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part004: {
        frame: {
          x: 501,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part005: {
        frame: {
          x: 501,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part006: {
        frame: {
          x: 0,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part007: {
        frame: {
          x: 501,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part000: {
        frame: {
          x: 1002,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part001: {
        frame: {
          x: 1002,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part002: {
        frame: {
          x: 1002,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part003: {
        frame: {
          x: 0,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part004: {
        frame: {
          x: 501,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part005: {
        frame: {
          x: 1002,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part006: {
        frame: {
          x: 1503,
          y: 0,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_white_part007: {
        frame: {
          x: 1503,
          y: 501,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part000: {
        frame: {
          x: 1503,
          y: 1002,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      },
      gameplay_chameleon_transform_color_part001: {
        frame: {
          x: 1503,
          y: 1503,
          w: 500,
          h: 500
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 500,
          h: 500
        },
        sourceSize: {
          w: 500,
          h: 500
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_9.png',
      size: {
        w: 2003,
        h: 2003
      },
      scale: 1
    }
  },
  [ASSET_KEY.ATLAS_SHEET_10]: {
    frames: {
      gameplay_booster_throw_screen_fx: {
        frame: {
          x: 0,
          y: 0,
          w: 750,
          h: 1600
        },
        rotated: false,
        trimmed: false,
        spriteSourceSize: {
          x: 0,
          y: 0,
          w: 750,
          h: 1600
        },
        sourceSize: {
          w: 750,
          h: 1600
        }
      }
    },
    meta: {
      app: 'https://www.leshylabs.com/apps/sstool/',
      version: 'Leshy SpriteSheet Tool v0.8.4',
      image: 'atlas_sheet_10.png',
      size: {
        w: 750,
        h: 1600
      },
      scale: 1
    }
  }
};

export function getAtlasCollection() {
  return atlasCollection;
}