import { ASSET_KEY } from '../Enum/Asset';
import { CampaignConfigV2 } from '../Interface/Campaign';
import { getAsset } from '../Util/AssetUtil';
import { AssetConfig } from '../Library/Interface/Asset';
import { ASSET_TYPE } from '../Library/Enum/Asset';

export function getAssetConfigs(config: CampaignConfigV2) {
  const assets = new Array<AssetConfig>();

  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_1,
    url: getAsset(config.image_100),
  });

  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_2,
    url: getAsset(config.image_129),
  });

  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_3,
    url: getAsset(config.image_168),
  });

  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_BUBBLE,
    url: getAsset('931675b806e7cff61f674cb0ccc7be15'),
  });

  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_MASCOT_IDLE,
    url: getAsset(config.image_51),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_MASCOT_THROW_STRAIGHT,
    url: getAsset(config.image_54),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_MASCOT_THROW_RIGHT,
    url: getAsset(config.image_53),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.GAMEPLAY_MASCOT_PANIC,
    url: getAsset(config.image_58),
  });

  // Atlas
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_1,
    url: getAsset('6119dca1932fa645d831b3ab57614677'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_2,
    url: getAsset('c254b07dcbd5741d1f0dcc4364f04e5f'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_3,
    url: getAsset('f5bd98e076dd747664e17d5fdb6188b9'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_4,
    url: getAsset('9ec210713f3f0090cd4c1e94f89b7880'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_5,
    url: getAsset('94a6f925a9cc210a3f35479347ebc147'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_6,
    url: getAsset('57f2fbd42ab2c22402c7436a2d7ddb40'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_7,
    url: getAsset('49ac748b9a8dd5a432bd296f4004ff87'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_8,
    url: getAsset('5718cc86e7d2b3978c1aa7e1bb5b8491'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_9,
    url: getAsset('ae343afc21723ab95b9ea50be06e0a48'),
  });
  assets.push({
    type: ASSET_TYPE.IMAGE,
    name: ASSET_KEY.ATLAS_SHEET_10,
    url: getAsset('55949eed9e6ae607a1692aadc3df1d0c'),
  });

  return assets;
}