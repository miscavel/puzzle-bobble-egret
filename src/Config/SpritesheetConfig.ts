import { ASSET_KEY } from '../Enum/Asset';
import { SpritesheetConfig } from '../Library/Interface/Spritesheet';

export function getSpritesheetConfigs() {
  const spritesheets = new Array<SpritesheetConfig>();

  spritesheets.push({
    textureSource: ASSET_KEY.GAMEPLAY_BUBBLE,
    frameWidth: 188,
    frameHeight: 188,
    padding: 1
  });

  spritesheets.push({
    textureSource: ASSET_KEY.GAMEPLAY_MASCOT_IDLE,
    frameWidth: 276,
    frameHeight: 276,
    padding: 0,
  });
  spritesheets.push({
    textureSource: ASSET_KEY.GAMEPLAY_MASCOT_THROW_STRAIGHT,
    frameWidth: 276,
    frameHeight: 276,
    padding: 0,
  });
  spritesheets.push({
    textureSource: ASSET_KEY.GAMEPLAY_MASCOT_THROW_RIGHT,
    frameWidth: 276,
    frameHeight: 276,
    padding: 0,
  });
  spritesheets.push({
    textureSource: ASSET_KEY.GAMEPLAY_MASCOT_PANIC,
    frameWidth: 276,
    frameHeight: 276,
    padding: 0,
  });

  return spritesheets;
}