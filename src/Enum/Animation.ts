export enum ANIMATION_KEY {
  BUBBLE_POP = 'bubble_pop',
  SAFETY_LINE_DANGER = 'safety_line_danger',
  MASCOT_IDLE = 'mascot_idle',
  MASCOT_THROW_STRAIGHT = 'mascot_throw_straight',
  MASCOT_THROW_RIGHT = 'mascot_throw_right',
  MASCOT_THROW_LEFT = 'mascot_throw_left',
  MASCOT_PANIC = 'mascot_panic',
}