export enum ASSET_KEY {
  GAMEPLAY_IMAGE_BACKGROUND_WORLD_1 = 'gameplay_image_background_world_1',
  GAMEPLAY_IMAGE_BACKGROUND_WORLD_2 = 'gameplay_image_background_world_2',
  GAMEPLAY_IMAGE_BACKGROUND_WORLD_3 = 'gameplay_image_background_world_3',
  GAMEPLAY_BUBBLE = 'gameplay_bubble',
  POP = 'pop_',
  GAMEPLAY_SAFETY_LINE = 'gameplay_safety_line',
  GAMEPLAY_SAFETY_LINE_DANGER = 'gameplay_safety_line_danger',
  GAMEPLAY_MASCOT_IDLE = 'gameplay_mascot_idle',
  GAMEPLAY_MASCOT_THROW_STRAIGHT = 'gameplay_mascot_throw_straight',
  GAMEPLAY_MASCOT_THROW_RIGHT = 'gameplay_mascot_throw_right',
  GAMEPLAY_MASCOT_PANIC = 'gameplay_mascot_panic',

  // Atlas
  ATLAS_SHEET_1 = 'atlas_sheet_1',
  ATLAS_SHEET_2 = 'atlas_sheet_2',
  ATLAS_SHEET_3 = 'atlas_sheet_3',
  ATLAS_SHEET_4 = 'atlas_sheet_4',
  ATLAS_SHEET_5 = 'atlas_sheet_5',
  ATLAS_SHEET_6 = 'atlas_sheet_6',
  ATLAS_SHEET_7 = 'atlas_sheet_7',
  ATLAS_SHEET_8 = 'atlas_sheet_8',
  ATLAS_SHEET_9 = 'atlas_sheet_9',
  ATLAS_SHEET_10 = 'atlas_sheet_10',

  ASSET_GROUP = 'asset_group', // For resource loading
}