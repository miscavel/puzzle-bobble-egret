export enum SCENE_KEY {
  NONE = 'none',
  PRELOAD = 'preload',
  GAMEPLAY = 'gameplay',
}