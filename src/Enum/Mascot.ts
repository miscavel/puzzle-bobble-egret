export enum MASCOT_STATE {
  IDLE = 'idle',
  PANIC = 'panic',
}

export enum MASCOT_EVENT {
  STATE_CHANGE = 'state_change',
}