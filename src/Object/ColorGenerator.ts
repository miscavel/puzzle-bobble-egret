import { ColorEnum } from '../Enum/Color';

export default class ColorGenerator {
  private availableColors: Array<ColorEnum> = [
    ColorEnum.BLUE,
    ColorEnum.YELLOW,
    ColorEnum.ORANGE,
    ColorEnum.RED
  ];

  public getRandomColor() {
    return this.availableColors[Math.floor(Math.random() * this.availableColors.length)] || ColorEnum.BLUE;
  }
}