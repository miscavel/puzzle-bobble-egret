import { ASSET_KEY } from '../../Enum/Asset';
import { DEPTH } from '../../Enum/Depth';
import Scene from '../../Library/Scene/Scene';
import ImageComponent from '../../Library/Component/ImageComponent';

export default class BackgroundImage extends ImageComponent {
  constructor(
    scene: Scene,
    x: number, 
    y: number,
    private world: number, 
    width?: number, 
    height?: number
  ) {
    super(
      scene,
      ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_1,
      undefined,
      x,
      y,
      width,
      height,
    );
    this.zIndex = DEPTH.BACKGROUND;

    this.updateTexture(this.getWorldTextureKey());
  }

  private getWorldTextureKey() {
    switch(this.world) {
      case 1: {
        return ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_1;
      }

      case 2: {
        return ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_2;
      }

      case 3: {
        return ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_3;
      }

      default: {
        return ASSET_KEY.GAMEPLAY_IMAGE_BACKGROUND_WORLD_1;
      }
    }
  }
}