import { ANIMATION_KEY } from '../../Enum/Animation';
import { ASSET_KEY } from '../../Enum/Asset';
import Scene from '../../Library/Scene/Scene';
import ImageComponent from '../../Library/Component/ImageComponent';

export default class MascotImage extends ImageComponent {
  constructor(
    scene: Scene, 
    x: number, 
    y: number, 
    width: number, 
    height: number
  ) {
    super(
      scene, 
      ASSET_KEY.GAMEPLAY_MASCOT_IDLE, 
      0,
      x, 
      y, 
      width, 
      height,
    );

      this.registerAnimations();
  }

  private registerAnimations() {
    const { scene } = this;
    const { animationManager } = scene;

    animationManager.registerAnimation({
      key: ANIMATION_KEY.MASCOT_IDLE,
      frames: animationManager.generateFrameNumbers(ASSET_KEY.GAMEPLAY_MASCOT_IDLE, 0, 1),
      frameRate: 3,
      yoyo: false,
      repeat: -1,
    });
    
    animationManager.registerAnimation({
      key: ANIMATION_KEY.MASCOT_THROW_STRAIGHT,
      frames: animationManager.generateFrameNumbers(ASSET_KEY.GAMEPLAY_MASCOT_THROW_STRAIGHT, 0, 1),
      frameRate: 8,
      yoyo: false,
      repeat: 0,
    });
    
    animationManager.registerAnimation({
      key: ANIMATION_KEY.MASCOT_THROW_RIGHT,
      frames: animationManager.generateFrameNumbers(ASSET_KEY.GAMEPLAY_MASCOT_THROW_RIGHT, 0, 1),
      frameRate: 8,
      yoyo: false,
      repeat: 0,
    });

    animationManager.registerAnimation({
      key: ANIMATION_KEY.MASCOT_THROW_LEFT,
      frames: animationManager.generateFrameNumbers(ASSET_KEY.GAMEPLAY_MASCOT_THROW_RIGHT, 0, 1),
      frameRate: 8,
      yoyo: false,
      repeat: 0,
      reverseX: true,
    });

    animationManager.registerAnimation({
      key: ANIMATION_KEY.MASCOT_PANIC,
      frames: animationManager.generateFrameNumbers(ASSET_KEY.GAMEPLAY_MASCOT_PANIC, 0, 1),
      frameRate: 3,
      yoyo: false,
      repeat: -1,
    });
  }

  public playIdle() {
    this.animator.play(ANIMATION_KEY.MASCOT_IDLE);
  }

  public playPanic() {
    this.animator.play(ANIMATION_KEY.MASCOT_PANIC);
  }

  public playShootStraight() {
    this.animator.play(ANIMATION_KEY.MASCOT_THROW_STRAIGHT);
  }

  public playShootRight() {
    this.animator.play(ANIMATION_KEY.MASCOT_THROW_RIGHT);
  }

  public playShootLeft() {
    this.animator.play(ANIMATION_KEY.MASCOT_THROW_LEFT);
  }
}