import { ANIMATION_KEY } from '../../Enum/Animation';
import { ASSET_KEY } from '../../Enum/Asset';
import { ColorEnum } from '../../Enum/Color';
import Scene from '../../Library/Scene/Scene';
import { getAtlasAnimationFrames } from '../../Library/Util/AtlasTool';
import ImageComponent from '../../Library/Component/ImageComponent';

export default class BubbleImage extends ImageComponent {
  constructor(
    scene: Scene,
    x: number, 
    y: number,
    private color: ColorEnum,
    width?: number, 
    height?: number
  ) {
    super(
      scene, 
      ASSET_KEY.GAMEPLAY_BUBBLE,
      0, 
      x, 
      y, 
      width, 
      height,
    );

    this.updateTexture(ASSET_KEY.GAMEPLAY_BUBBLE, this.getColorFrameKey());

    this.registerPopAnimation();
  }

  private registerPopAnimation() {
    const { scene } = this;
    const { animationManager } = scene;

    animationManager.registerAnimation({
      key: this.getPopAnimationKey(),
      frames: getAtlasAnimationFrames(scene, this.getPopTexturePrefix(), 1, 4),
      frameRate: 16,
      yoyo: false,
      repeat: 0
    });
  }

  private getPopAnimationKey() {
    return `${ANIMATION_KEY.BUBBLE_POP}${this.color}`;
  }

  private getPopTexturePrefix() {
    return `${ASSET_KEY.POP}${this.color}`;
  }

  public playPopAnimation(delay = 0) {
    this.animator.play(this.getPopAnimationKey(), delay);
  }

  private getColorFrameKey() {
    const {
      BLUE,
      ORANGE,
      YELLOW,
      GREEN,
      PINK,
      STEEL,
      PURPLE,
      RED,
    } = ColorEnum;

    switch (this.color) {
      case BLUE: {
        return 0;
      }

      case ORANGE: {
        return 1;
      }

      case YELLOW: {
        return 2;
      }

      case GREEN: {
        return 6;
      }

      case PINK: {
        return 7;
      }

      case STEEL: {
        return 8;
      }

      case PURPLE: {
        return 12;
      }

      case RED: {
        return 13;
      }

      default: {
        return 0;
      }
    }
  }

  public updateColor(color: ColorEnum) {
    this.color = color;

    this.updateTexture(ASSET_KEY.GAMEPLAY_BUBBLE, this.getColorFrameKey());
  }
}