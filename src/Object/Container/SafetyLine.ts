import { SAFETY_LINE_STATE } from '../../Enum/SafetyLine';
import Scene from '../../Library/Scene/Scene';
import { setOrigin } from '../../Library/Util/DisplayObjectUtil';
import SafetyLineImage from '../Image/SafetyLineImage';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class SafetyLine extends ContainerComponent {
  private image: SafetyLineImage;

  private state: SAFETY_LINE_STATE = SAFETY_LINE_STATE.SAFE;

  constructor(scene: Scene, x: number, y: number) {
    super(scene, x, y);
    scene.addChild(this);

    this.addChild(
      setOrigin(
        this.image = new SafetyLineImage(scene, 0, 0, scene.stage.width, scene.stage.width * 0.15),
        0.5,
        0.5
      )
    );
  }

  public setState(state: SAFETY_LINE_STATE) {
    if (state === this.state) return;

    switch(state) {
      case SAFETY_LINE_STATE.DANGER: {
        this.image.playDangerAnimation();
        break;
      }

      case SAFETY_LINE_STATE.SAFE: {
        this.image.stopDangerAnimation();
        break;
      }

      default: {
        this.image.stopDangerAnimation();
        break;
      }
    }

    this.state = state;
  }
}