import { ANIMATION_EVENT } from '../../Library/Enum/Animation';
import { MASCOT_EVENT, MASCOT_STATE } from '../../Enum/Mascot';
import Scene from '../../Library/Scene/Scene';
import { setOrigin } from '../../Library/Util/DisplayObjectUtil';
import MascotImage from '../Image/MascotImage';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class Mascot extends ContainerComponent {
  private image: MascotImage;

  private state: MASCOT_STATE = MASCOT_STATE.IDLE;

  private isShooting = false;
  
  constructor(scene: Scene, x: number, y: number) {
    super(scene, x, y);
    scene.addChild(this);

    this.addChild(
      setOrigin(
        this.image = new MascotImage(
          scene,
          0,
          0,
          scene.stage.stageWidth * 0.35,
          scene.stage.stageWidth * 0.35,
        ),
        0.5,
        0.5,
      )
    );

    this.image.playIdle();
    this.image.addEventListener(ANIMATION_EVENT.COMPLETE, () => {
      this.isShooting = false;
      this.playStateAnimation();
    }, this);

    this.addEventListener(MASCOT_EVENT.STATE_CHANGE, () => {
      this.playStateAnimation();
    }, this);
  }

  public setState(state: MASCOT_STATE) {
    this.state = state;
    this.dispatchEvent(new egret.Event(MASCOT_EVENT.STATE_CHANGE, false, false, { state }));
  }

  public playShootAnimation(directionX: number, directionY: number) {
    this.isShooting = true;
    if (directionX <= -0.3) {
      this.image.playShootLeft();
    } else if (directionX > 0.3) {
      this.image.playShootRight();
    } else {
      this.image.playShootStraight();
    }
  }

  private playStateAnimation() {
    if (this.isShooting) return;

    switch(this.state) {
      case MASCOT_STATE.IDLE: {
        this.image.playIdle();
        break;
      }

      case MASCOT_STATE.PANIC: {
        this.image.playPanic();
        break;
      }

      default: {
        break;
      }
    }
  }
}