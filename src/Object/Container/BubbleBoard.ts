import { BUBBLE_BOARD_EVENT } from '../../Enum/BubbleBoard';
import { ColorEnum } from '../../Enum/Color';
import { Vector2 } from '../../Library/Interface/Vector';
import Scene from '../../Library/Scene/Scene';
import { DEG_TO_RAD } from '../../Library/Util/Constants';
import { getDirectionBetween, getDistanceBetween } from '../../Library/Util/MathUtil';
import { getEntries } from '../../Library/Util/ObjectUtil';
import ColorGenerator from '../ColorGenerator';
import Bubble from './Bubble';
import BubbleSlot from './BubbleSlot';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export type SnapBubbleSlotData = {
  distance: number;
  bubbleSlot?: BubbleSlot;
}

export default class BubbleBoard extends ContainerComponent {
  private readonly EVEN_COLUMN_COUNT = 9;

  private readonly ODD_COLUMN_COUNT = 8;

  private readonly POP_MATCH_COUNT = 3;

  private readonly TOTAL_ROW = 13;

  private readonly GENERATED_ROW = 8;

  private readonly DANGER_ROW_THRESHOLD = 3;

  private readonly MAX_DISTANCE = 490110;

  private bubbleSlots: { [key: string]: BubbleSlot } = {};

  constructor(
    scene: Scene,
    x: number,
    y: number,
    private colorGenerator: ColorGenerator
  ) {
    super(scene, x, y);
    scene.addChild(this);

    this.generateBubbleSlots();
  }

  private generateBubbleSlots() {
    const { scene, TOTAL_ROW } = this;
    for (let row = 0; row < TOTAL_ROW; row++) {
      for (let col = 0; col < this.getColumnCount(row); col++) {
        const key = this.getBubbleSlotKey(row, col);
        const { x, y } = this.getBubbleSlotPositionFromIndex(row, col);
        const slotSize = this.getBubbleSlotRadius() * 2;
        this.addChild(
          this.assignBubbleSlot(
            key,
            new BubbleSlot(
              scene,
              x,
              y,
              row,
              col,
              slotSize,
              slotSize,
              this.generateBubbleSlotColor(row)
            )
          )
        );
      }
    }

    this.reloadBubbleSlotsNeighbors();
  }

  private isEvenRow(row) {
    return row % 2 === 0;
  }

  private getColumnCount(row) {
    if (this.isEvenRow(row)) {
      return this.EVEN_COLUMN_COUNT;
    }

    return this.ODD_COLUMN_COUNT;
  }

  private getBubbleSlotKey(row: number, col: number) {
    return `${row}|${col}`;
  }

  private getBubbleSlotByKey(key: string) {
    return this.bubbleSlots[key];
  }

  private assignBubbleSlot(key: string, bubbleSlot: BubbleSlot) {
    this.bubbleSlots[key] = bubbleSlot;

    return bubbleSlot;
  }

  private getBubbleSlotPositionFromIndex(row: number, col: number) {
    return {
      x: this.getBubbleStartX(row) + col * this.getBubbleSlotXDistance(),
      y: this.getBubbleStartY() + row * this.getBubbleSlotYDistance(),
    } as Vector2;
  }

  private getBubbleSlotKeyFromPosition(x: number, y: number) {
    const inBoardX = this.getInBoardX(x);
    const inBoardY = this.getInBoardY(y);

    const row = Math.round((inBoardY - this.getBubbleStartY()) / this.getBubbleSlotYDistance());
    const col = Math.round((inBoardX - this.getBubbleStartX(row)) / this.getBubbleSlotXDistance());

    return this.getBubbleSlotKey(row, col);
  }

  public getBubbleSlotFromPosition(x: number, y: number) {
    return this.getBubbleSlotByKey(this.getBubbleSlotKeyFromPosition(x, y));
  }

  private getBubbleSlotRadius() {
    return (this.scene.stage.stageWidth / this.EVEN_COLUMN_COUNT) * 0.5;
  }

  public getInBoardX(x: number) {
    return x - this.x;
  }

  public getInBoardY(y: number) {
    return y - this.y;
  }

  private getBubbleStartX(row: number) {
    if (this.isEvenRow(row)) {
      return this.getBubbleSlotRadius();;
    }

    return this.getBubbleSlotRadius() * 2;
  }

  private getBubbleStartY() {
    return this.getBubbleSlotRadius();
  }

  private getBubbleSlotXDistance() {
    return this.getBubbleSlotRadius() * 2;
  }

  private getBubbleSlotYDistance() {
    return Math.tan(60 * DEG_TO_RAD) * this.getBubbleSlotRadius();
  }

  private reloadBubbleSlotsNeighbors() {
    getEntries(this.bubbleSlots).forEach(([key, bubbleSlot]) => {
      this.registerNeighbors(bubbleSlot as BubbleSlot);
    });
  }

  private registerNeighbors(bubbleSlot: BubbleSlot) {
    const neighborOffsets = [
      { x: -this.getBubbleSlotXDistance(), y: 0 },
      { x: -this.getBubbleSlotXDistance() * 0.5, y: -this.getBubbleSlotYDistance() },
      { x: this.getBubbleSlotXDistance() * 0.5, y: -this.getBubbleSlotYDistance() },
      { x: this.getBubbleSlotXDistance() * 0.5, y: 0 },
      { x: this.getBubbleSlotXDistance() * 0.5, y: this.getBubbleSlotYDistance() },
      { x: -this.getBubbleSlotXDistance() * 0.5, y: this.getBubbleSlotYDistance() }
    ];

    neighborOffsets.forEach((neighborOffset) => {
      const bubbleSlotKey = this.getBubbleSlotKeyFromPosition(
        bubbleSlot.x + neighborOffset.x,
        bubbleSlot.y + neighborOffset.y
      );

      bubbleSlot.registerNeighbor(this.getBubbleSlotByKey(bubbleSlotKey));
    });
  }

  private popBubbleSlot(bubbleSlot: BubbleSlot) {
    const { POP_MATCH_COUNT } = this;

    const matchingSlots = getEntries(this.getConnectedSlotsOfMatchingColor(bubbleSlot, {}));

    if (matchingSlots.length >= POP_MATCH_COUNT) {
      matchingSlots.forEach(([_, slot], index, array) => {
        const delay = (array.length - 1 - index) * 50;
        (slot as BubbleSlot).pop(delay);
      });
      return true;
    }
    return false;
  }

  private getConnectedSlotsOfMatchingColor(
    bubbleSlot: BubbleSlot,
    connectedSlots: { [key: string]: BubbleSlot },
  ): { [key: string]: BubbleSlot } {
    const { hashCode } = bubbleSlot;

    if (connectedSlots[hashCode]) return connectedSlots;

    connectedSlots[hashCode] = bubbleSlot;

    return bubbleSlot.getNeighbors().reduce((result, neighbor) => {
      if (bubbleSlot.matchSlot(neighbor)) {
        return this.getConnectedSlotsOfMatchingColor(neighbor, connectedSlots);
      }
      return result;
    }, connectedSlots);
  }

  private getConnectedSlotsWithBubble(
    bubbleSlot: BubbleSlot,
    connectedSlots: { [key: string]: BubbleSlot },
  ): { [key: string]: BubbleSlot } {
    const { hashCode } = bubbleSlot;

    if (connectedSlots[hashCode]) return connectedSlots;

    connectedSlots[hashCode] = bubbleSlot;

    return bubbleSlot.getNeighbors().reduce((result, neighbor) => {
      if (neighbor.getBubble() !== undefined) {
        return this.getConnectedSlotsWithBubble(neighbor, connectedSlots);
      }
      return result;
    }, connectedSlots);
  }

  private dropFloatingSlots() {
    this.getFloatingSlots().forEach((slot) => {
      slot.drop();
    });
  }

  private getFloatingSlots() {
    const anchorBubbleSlot = this.getBubbleSlotByKey(this.getBubbleSlotKey(0, 0));
    const anchoredBubbleSlots = this.getConnectedSlotsWithBubble(anchorBubbleSlot, {});

    return getEntries(this.bubbleSlots).filter(([_, bubbleSlot]) => {
      const { hashCode } = (bubbleSlot as BubbleSlot);

      return !anchoredBubbleSlots[hashCode];
    }).map(([_, bubbleSlot]) => {
      return bubbleSlot as BubbleSlot;
    });
  }

  private generateBubbleSlotColor(row: number) {
    if (row === 0) {
      return ColorEnum.STEEL;
    }

    if (row >= this.GENERATED_ROW) {
      return undefined;
    }

    return this.colorGenerator.getRandomColor();
  }

  public getSnapBubbleSlot(x: number, y: number, collidedBubbleSlot?: BubbleSlot) {
    const { MAX_DISTANCE } = this;

    const inBoardX = this.getInBoardX(x);
    const inBoardY = this.getInBoardY(y);

    const defaultSnapBubbleSlotData = { distance: MAX_DISTANCE, bubbleSlot: undefined } as SnapBubbleSlotData;

    const snapBubbleSlotData = collidedBubbleSlot?.getNeighbors().reduce((result, neighbor) => {
      const { x: slotX, y: slotY } = neighbor;
      const distance = getDistanceBetween(inBoardX, inBoardY, slotX, slotY);
      if (distance < result.distance) {
        return {
          distance,
          bubbleSlot: neighbor
        };
      }
      return result;
    }, defaultSnapBubbleSlotData) || defaultSnapBubbleSlotData;

    return snapBubbleSlotData.bubbleSlot;
  }

  public snapBubbleAt(
    x: number,
    y: number,
    color: ColorEnum,
    collidedBubbleSlot?: BubbleSlot
  ) {
    const snapBubbleSlot = this.getSnapBubbleSlot(x, y, collidedBubbleSlot);

    if (!collidedBubbleSlot || !snapBubbleSlot) return;

    snapBubbleSlot.spawnBubbleOfColor(color);
    const bubble = snapBubbleSlot.getBubble();
    const { x: incomingDirectionX, y: incomingDirectionY } = getDirectionBetween(
      this.getInBoardX(x),
      this.getInBoardY(y),
      collidedBubbleSlot.x,
      collidedBubbleSlot.y,
    );

    if (this.popBubbleSlot(snapBubbleSlot)) {
      this.dropFloatingSlots();
      this.animateSnap(
        bubble, 
        x, 
        y,
        bubble.x,
        bubble.y, 
        incomingDirectionX, 
        incomingDirectionY,
      );
    } else {
      const { x: localX, y: localY } = snapBubbleSlot.globalToLocal(x, y);
      this.animateSnap(
        bubble, 
        localX, 
        localY,
        0,
        0, 
        incomingDirectionX, 
        incomingDirectionY,
      );
    }
    this.jiggleNeighbors(snapBubbleSlot);
    this.checkBoardDanger();
    this.checkHitLastRow(snapBubbleSlot);
    this.checkClearAllBubbles();
  }

  private animateSnap(
    bubble: Bubble,
    fromX: number,
    fromY: number,
    toX: number,
    toY: number,
    incomingDirectionX: number, 
    incomingDirectionY: number,
  ) {
    if (!bubble) return;

    bubble.setPosition(fromX, fromY);

    const snapTween = egret.Tween.get(bubble);
    snapTween.to({ 'x': toX -incomingDirectionX * 8, 'y': toY -incomingDirectionY * 8 }, 50, egret.Ease.quadOut);
    snapTween.to({ 'x': toX, 'y': toY }, 150, egret.Ease.quadIn);
  }

  public jiggleNeighbors(bubbleSlot: BubbleSlot) {
    const firstNeighbors = bubbleSlot.getNeighbors();
    const secondNeighbors = firstNeighbors.reduce((result, neighbor) => {
      result.push(...neighbor.getNeighbors());
      return result;
    }, new Array<BubbleSlot>());

    const jiggledMap: { [key: string]: boolean } = {};
    firstNeighbors.forEach((neighbor) => {
      const { hashCode } = neighbor;
      const bubble = neighbor.getBubble();
      if (
        jiggledMap[hashCode] ||
        !bubble
      ) return;

      jiggledMap[hashCode] = true;

      this.animateJiggle(bubble, bubble.x, bubble.y, bubble.x, bubble.y - 8);
    });
    secondNeighbors.forEach((neighbor) => {
      const { hashCode } = neighbor;
      const bubble = neighbor.getBubble();
      if (
        jiggledMap[hashCode] ||
        !bubble
      ) return;

      jiggledMap[hashCode] = true;

      this.animateJiggle(bubble, bubble.x, bubble.y, bubble.x, bubble.y - 4);
    });
  }

  private animateJiggle(
    bubble: Bubble,
    fromX: number,
    fromY: number,
    toX: number,
    toY: number,
  ) {
    if (!bubble) return;

    const jiggleTween = egret.Tween.get(bubble);

    jiggleTween.to({ 'x': toX, 'y': toY }, 100, egret.Ease.quadOut);
    jiggleTween.to({ 'x': fromX, 'y': fromY }, 100, egret.Ease.quadIn);
  }

  private checkBoardDanger() {
    let danger = false;
    const dangerRow = this.TOTAL_ROW - 1 - this.DANGER_ROW_THRESHOLD;
    for (let col = 0; col < this.getColumnCount(dangerRow); col++) {
      const bubbleSlot = this.getBubbleSlotByKey(this.getBubbleSlotKey(dangerRow, col));

      if (bubbleSlot.getBubble()) {
        danger = true;
        break;
      }
    }

    if (danger) {
      this.dispatchEvent(new egret.Event(BUBBLE_BOARD_EVENT.BOARD_IN_DANGER));
    } else {
      this.dispatchEvent(new egret.Event(BUBBLE_BOARD_EVENT.BOARD_IN_SAFETY));
    }
  }

  private checkHitLastRow(bubbleSlot: BubbleSlot) {
    if (
      bubbleSlot.getBubble() &&
      bubbleSlot.row === this.TOTAL_ROW - 1
    ) {
      this.dispatchEvent(new egret.Event(BUBBLE_BOARD_EVENT.HIT_LAST_ROW));
    }
  }

  private checkClearAllBubbles() {
    let cleared = true;
    const firstRow = 1;
    for (let col = 0; col < this.getColumnCount(firstRow); col++) {
      const bubbleSlot = this.getBubbleSlotByKey(this.getBubbleSlotKey(firstRow, col));

      if (bubbleSlot.getBubble()) {
        cleared = false;
        break;
      }
    }
    
    if (cleared) {
      this.dispatchEvent(new egret.Event(BUBBLE_BOARD_EVENT.CLEAR_ALL_BUBBLES))
    }
  }
}