import { CampaignDataV2, CampaignResponseV2 } from '../Interface/Campaign';
import ApiServices from './ApiServices';

export const getCampaignDataV2 = async (
  campaignId: string,
): Promise<CampaignDataV2> => {
  try {
    const { campaignV2 } = ApiServices.routes;
    const fetchRes = await ApiServices.get(campaignV2(campaignId));
    const response: CampaignResponseV2 = await fetchRes.json();

    if (!ApiServices.isOK(fetchRes)) throw response;

    const { data } = response;
    return data;
  } catch (e) {
    throw e;
  }
};