import { LOADING_BAR_EVENT } from '../Enum/LoadingBar';
import Scene from '../Scene/Scene';
import { setOrigin } from '../Util/DisplayObjectUtil';
import ContainerComponent from './ContainerComponent';

export default class LoadingBar extends ContainerComponent {
  private progressBorder: egret.Shape;

  private progressMask: egret.Shape;

  private progressBG: egret.Shape;

  private progressBar: egret.Shape;

  private progressText: egret.TextField;

  private readonly MIN_PROGRESS = 0;

  private readonly MAX_PROGRESS = 100;

  /**
   * Ranges from 0 to 100
   */
  private progress = 0;
  
  constructor(
    scene: Scene,
    x: number, 
    y: number,
    private progressBarWidth: number,
    private progressBarHeight: number,
    private progressBorderColor = 0xf8d48e,
    private progressBGColor = 0x000000,
    private progressBarColor = 0xee4d2d,
  ) {
    super(scene, x, y);

    this.addChild(this.progressBorder = this.createProgressBorder());
    this.addChild(this.progressMask = this.createProgressMask());
    this.addChild(this.progressBG = this.createProgressBG());
    this.addChild(this.progressBar = this.createProgressBar());
    this.addChild(this.progressText = this.createProgressText());

    this.addEventListener(LOADING_BAR_EVENT.UPDATE, this.updateProgressBar, this);
    this.addEventListener(LOADING_BAR_EVENT.UPDATE, this.updateProgressText, this);

    this.updateProgress(this.MIN_PROGRESS);
  }

  private createProgressBorder() {
    const { progressBorderColor, progressBarWidth, progressBarHeight } = this;

    const progressBorder = new egret.Shape();
    progressBorder.graphics.beginFill(progressBorderColor);
    progressBorder.graphics.drawRoundRect(
      0, 
      0, 
      progressBarWidth, 
      progressBarHeight, 
      this.getEllipseWidth(),
    );
    progressBorder.graphics.endFill();
    setOrigin(progressBorder, 0.5, 0.5);

    return progressBorder;
  }

  private createProgressMask() {
    const progressMask = new egret.Shape();
    progressMask.graphics.drawRoundRect(
      0,
      0,
      this.getMaskWidth(),
      this.getMaskHeight(),
      this.getMaskEllipseWidth(),
    );
    progressMask.graphics.endFill();
    setOrigin(progressMask, 0.5, 0.5);
    progressMask.blendMode = egret.BlendMode.ERASE;

    return progressMask;
  }

  private createProgressBG() {
    const { progressBGColor } = this;

    const progressBG = new egret.Shape();
    progressBG.graphics.beginFill(progressBGColor);
    progressBG.graphics.drawRoundRect(
      0,
      0,
      this.getMaskWidth(),
      this.getMaskHeight(),
      this.getMaskEllipseWidth(),
    );
    progressBG.graphics.endFill();
    setOrigin(progressBG, 0.5, 0.5);

    return progressBG;
  }

  private createProgressBar() {
    const { progressBarColor } = this;

    const progressBar = new egret.Shape();
    progressBar.graphics.beginFill(progressBarColor);
    progressBar.graphics.drawRoundRect(
      0,
      0,
      this.getMaskWidth(),
      this.getMaskHeight(),
      this.getMaskEllipseWidth(),
    );
    progressBar.graphics.endFill();
    setOrigin(progressBar, 0, 0);

    progressBar.x = -this.getMaskWidth() * 0.5;
    progressBar.y = -this.getMaskHeight() * 0.5;

    return progressBar;
  }

  private createProgressText() {
    const progressText = new egret.TextField();
    progressText.width = this.getProgressTextWidth();
    progressText.height = this.getProgressTextHeight();
    progressText.textAlign = "center";

    setOrigin(progressText, 0.5, 0.25);

    return progressText;
  }

  private getEllipseWidth() {
    return this.progressBarWidth * 0.1;
  }

  private getMaskWidth() {
    return this.progressBarWidth * 0.95;
  }

  private getMaskHeight() {
    return this.progressBarHeight * 0.8;
  }

  private getMaskEllipseWidth() {
    return this.getEllipseWidth() * 0.95;
  }

  private getProgressTextWidth() {
    return this.progressBarWidth * 0.2;
  }

  private getProgressTextHeight() {
    return this.progressBarHeight * 0.9;
  }

  public getProgressPercentage(progress: number) {
    const { MIN_PROGRESS, MAX_PROGRESS } = this;

    return (progress - MIN_PROGRESS) * 100 / (MAX_PROGRESS - MIN_PROGRESS);
  }

  public updateProgress(progress: number) {
    const { MIN_PROGRESS, MAX_PROGRESS } = this;

    this.progress = Math.min(
      Math.max(progress, MIN_PROGRESS),
      MAX_PROGRESS
    );

    this.dispatchEvent(
      new egret.Event(
        LOADING_BAR_EVENT.UPDATE, 
        false, 
        false, 
        { 
          progressPercentage: this.getProgressPercentage(this.progress)
        }
      )
    );

    this.checkComplete(progress);
  }

  private checkComplete(progress: number) {
    const isComplete = progress >= this.MAX_PROGRESS;
    if (isComplete) {
      this.dispatchEvent(
        new egret.Event(
          LOADING_BAR_EVENT.COMPLETE
        )
      );
    }
  }

  private updateProgressBar(eventData: egret.Event) {
    const { progressPercentage } = eventData.data;

    const { progressBar, progressBarColor } = this;

    progressBar.graphics.clear();
    progressBar.graphics.beginFill(progressBarColor);
    progressBar.graphics.drawRoundRect(
      0,
      0,
      this.getMaskWidth() * (progressPercentage / 100),
      this.getMaskHeight(),
      this.getMaskEllipseWidth(),
    );
    progressBar.graphics.endFill();
  }

  private updateProgressText(eventData: egret.Event) {
    const { progressPercentage } = eventData.data;

    this.progressText.text = `${Math.floor(progressPercentage)}%`;
  }
}