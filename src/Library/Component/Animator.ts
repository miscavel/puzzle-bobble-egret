import { ANIMATION_EVENT } from '../Enum/Animation';
import { SCENE_EVENT } from '../Enum/Scene';
import { Animation } from '../Interface/Animation';
import Scene from '../Scene/Scene';

export default class Animator extends egret.EventDispatcher {
  private animation: Animation;

  private activeFrame = Number.MIN_VALUE;

  private frameIncrement = 1;

  private repeat = 0;

  private lastFrameTimestamp = Date.now();

  constructor(private scene: Scene) {
    super();
    scene.registerEventListener(SCENE_EVENT.UPDATE, this.update, this);
  }

  public isPlaying() {
    return this.activeFrame !== Number.MIN_VALUE;
  }

  public getCurrentAnimationKey() {
    if (!this.isPlaying()) return undefined;

    return this.animation.key;
  }

  public play(key: string, delay = 0) {
    const animation = this.scene.animationManager.getAnimation(key);

    if (
      !animation ||
      this.getCurrentAnimationKey() === key
    ) return;

    this.stop();

    this.animation = animation;
    this.repeat = animation.repeat;
    this.resetAnimationFrame();
    this.lastFrameTimestamp += delay;

    this.dispatchEvent(new egret.Event(ANIMATION_EVENT.START));
  }

  public stop() {
    this.activeFrame = Number.MIN_VALUE;
  }

  private resetAnimationFrame(isRepeat = false, yoyo = false, reverse = false) {
    if (isRepeat && yoyo) {
      if (reverse) {
        this.activeFrame = this.animation.frames.length - 2;
        this.frameIncrement = -1;
      } else {
        this.activeFrame = 1;
        this.frameIncrement = 1;
      }
    } else {
      this.activeFrame = 0;
      this.frameIncrement = 1;
    }
    this.lastFrameTimestamp = Date.now() - this.getMSFromFrameRate(this.animation.frameRate);
  }

  private update() {
    if (this.isPlaying()) {
      const { frameRate, frames, yoyo, reverseX, reverseY } = this.animation;

      const elapsed = Date.now() - this.lastFrameTimestamp;
      if (elapsed >= this.getMSFromFrameRate(frameRate)) {
        const frameToRender = frames[this.activeFrame];
        if (frameToRender) {
          this.dispatchEvent(new egret.Event(ANIMATION_EVENT.UPDATE, false, false, { frameToRender, reverseX, reverseY }));
          this.activeFrame += this.frameIncrement;
          this.lastFrameTimestamp = Date.now();
        } else {
          if (this.repeat === 0) {
            this.stop();
            this.dispatchEvent(new egret.Event(ANIMATION_EVENT.COMPLETE));
          } else {
            this.repeat -= 1;
            this.resetAnimationFrame(true, yoyo, this.frameIncrement === 1);
          }
        }
      }
    }
  }

  private getMSFromFrameRate(frameRate: number) {
    return 1000 / frameRate;
  }
}