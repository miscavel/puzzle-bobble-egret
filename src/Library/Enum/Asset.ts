export enum ASSET_TYPE {
  IMAGE = 'image',
  SPRITESHEET = 'spritesheet',
  AUDIO = 'audio',
  BITMAP_FONT = 'bitmap_font',
  ATLAS = 'atlas',
}