export enum ANIMATION_EVENT {
  START = 'start',
  UPDATE = 'update',
  COMPLETE = 'complete',
}