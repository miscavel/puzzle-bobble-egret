import Scene from '../Scene/Scene';
import { getEntries } from '../Util/ObjectUtil';
import { SceneInstance } from '../Scene/SceneInstance';

export default class SceneManager {
  private scenes: { [key: string]: SceneInstance } = {};

  constructor(
    private main: egret.DisplayObjectContainer
  ) {
    
  }

  public registerScene(scene: Scene) {
    const { key } = scene;
    if (!this.scenes[key]) {
      this.scenes[key] = new SceneInstance(scene);
      this.main.addChild(scene);
    }
  }

  public start(sceneKey: string) {
    getEntries(this.scenes).forEach(([key, _]) => {
      this.stop(key as string);
    });

    this.run(sceneKey);
  }

  public stop(sceneKey: string) {
    this.scenes[sceneKey]?.stop();
  }

  public run(sceneKey: string) {
    this.scenes[sceneKey]?.start();
  }
}