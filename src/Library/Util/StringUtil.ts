export function getZeroPaddedNum(num: number, zeroPad: number, zeroPadChar = '0') {
  const numString = num.toString();
  const missingZeros = zeroPad - numString.length;

  let zeros = '';
  for (let i = 0; i < missingZeros; i++) {
    zeros = `${zeros}${zeroPadChar}`;
  }

  return `${zeros}${numString}`;
}