export function getEntries<T>(object: { [key: string]: T }) {
  return Object.keys(object).map((key) => {
    return [key, object[key]];
  });
}