export function setOrigin(
  displayObject: egret.DisplayObject,
  originX: number,
  originY: number,
) {
  displayObject.anchorOffsetX = originX * displayObject.width;
  displayObject.anchorOffsetY = originY * displayObject.height;
  
  return displayObject;
}