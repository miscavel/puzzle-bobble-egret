import { SCENE_EVENT, SCENE_INSTANCE_STATE } from '../Enum/Scene';
import { STAGE_EVENT } from '../Enum/Stage';
import Scene from './Scene';

export class SceneInstance {
  private state = SCENE_INSTANCE_STATE.STOPPED;

  constructor(
    private scene: Scene
  ) {
  }

  public start() {
    if (this.isRunning())
      return;

    this.scene.onStart();
    this.scene.stage.addEventListener(STAGE_EVENT.UPDATE, this.update, this);
    this.setState(SCENE_INSTANCE_STATE.RUNNING);
  }

  public stop() {
    if (!this.isRunning())
      return;

    this.scene.dispatchEvent(new egret.Event(SCENE_EVENT.SHUTDOWN));
    this.scene.onStop();
    this.scene.stage.removeEventListener(STAGE_EVENT.UPDATE, this.update, this);
    this.setState(SCENE_INSTANCE_STATE.STOPPED);
  }

  public setState(state: SCENE_INSTANCE_STATE) {
    this.state = state;
  }

  public isRunning() {
    return this.state === SCENE_INSTANCE_STATE.RUNNING;
  }

  private update() {
    this.scene.dispatchEvent(new egret.Event(SCENE_EVENT.UPDATE));
  }
}
